#define PLUGIN_AUTHOR ".#Zipcore"
#define PLUGIN_NAME "Team Builder"
#define PLUGIN_VERSION "1.0"
#define PLUGIN_DESCRIPTION "Viewer/Sub/Private Team build helper based on ranks players can assign to themselfes"
#define PLUGIN_URL "zipcore.net"

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <clientprefs>

#include <cstrike>

#include <smlib> // Collection of stocks
#include <csgocolors> // Chat colors for CS:GO

#undef REQUIRE_PLUGIN

#include <chat-processor>

#include <zcore/zcore_mysql> // Handles listen overrides
bool g_pZcoreMysql = false;

#define CPREFIX "{darkred}[TB]{yellow}"

/* Database */

bool g_bConnected;

/* Player */

char g_sAuth[MAXPLAYERS + 1][32];
bool g_bAuthed[MAXPLAYERS + 1];
bool g_bLoadedSQL[MAXPLAYERS + 1];
						
int g_iELO[MAXPLAYERS+1] = {-1, ...};
int g_iRank[MAXPLAYERS+1] = {-1, ...};

/* Ready */

bool g_bWait;

/* ELO Preset*/

int g_iRankELO[18] =  {	2200, 
					2000, 
					1900, 
					1800, 
					1700, 
					1600, 
					1500, 
					1400, 
					1300, 
					1200, 
					1100, 
					1000, 
					900, 
					800, 
					600, 
					400, 
					200, 
					1 };

char g_sRanks[18][] =  {	"The Global Elite", 
						"Supreme Master First Class", 
						"Legendary Eagle Master", 
						"Legendary Eagle", 
						"Distinguished Master Guardian", 
						"Master Guardian Elite", 
						"Master Guardian II", 
						"Master Guardian I", 
						"Gold Nova Master", 
						"Gold Nova III", 
						"Gold Nova II", 
						"Gold Nova I", 
						"Silver Elite Master", 
						"Silver Elite", 
						"Silver IV", 
						"Silver III", 
						"Silver II", 
						"Silver I" };

char g_sRanksShort[18][] =  {	"TGE", 
						"SMFC", 
						"LEM", 
						"LE", 
						"DMG", 
						"MGE", 
						"MG2", 
						"MG1", 
						"GNM", 
						"GN3", 
						"GN2", 
						"GN1", 
						"SEM", 
						"SE", 
						"S4", 
						"S3", 
						"S2", 
						"S1" };

/* Macros */

#define LoopIngameClients(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1))

#define LoopArray(%1,%2) for(int %1=0;%1<GetArraySize(%2);++%1)

/* Handle Plugins */

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	RegPluginLibrary("teambuilder");
	
	return APLRes_Success;
}

public void OnLibraryAdded(const char[] name)
{
	if (StrEqual(name, "zcore-mysql"))
		g_pZcoreMysql = true;
}

public void OnLibraryRemoved(const char[] name)
{
	if (StrEqual(name, "zcore-mysql"))
		g_pZcoreMysql = false;
}

/* Plugin Start */

public void OnPluginStart()
{
	/* Hooks */
	
	HookEvent("player_team", Event_PlayerTeam);
	HookEvent("player_death", Event_PlayerDeath);
	
	/* Commands */
	
	RegAdminCmd("tb", Cmd_Teams, ADMFLAG_GENERIC, "Checks if all players selected a rank and builds teams.");
	RegAdminCmd("sm_tb", Cmd_Teams, ADMFLAG_GENERIC, "Checks if all players selected a rank and builds teams.");
	
	/* Mysql */
	ConnectDB();
	
	LoopIngameClients(client)
	{
		OnClientPutInServer(client);
		AuthPlayer(client);
	}
}

public void OnMapEnd()
{
	ResetAuthTimers();
}

public void OnClientPostAdminCheck(int client)
{
	AuthPlayer(client);
}

public void Event_PlayerTeam(Handle event, const char[] name, bool db)
{
	int iClient = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if(!IsClientSourceTV(iClient))
		CS_SetClientClanTag(iClient, g_iRank[iClient] == -1 ? "???" : g_sRanksShort[g_iRank[iClient]]);
}

public Action OnClientCommandKeyValues(int client, KeyValues kv)
{
	char sCmd[64];
	
	if (kv.GetSectionName(sCmd, sizeof(sCmd)) && StrEqual(sCmd, "ClanTagChanged", false))
		return Plugin_Handled;
	
	return Plugin_Continue;
}  

public void OnClientPutInServer(int iClient)
{
	if(IsFakeClient(iClient))
	{
		g_iRank[iClient] = GetRandomInt(8, 17);
		CS_SetClientClanTag(iClient, g_sRanksShort[g_iRank[iClient]]);
		return;
	}
}

public void OnClientCookiesCached(int iClient)
{
	if(IsClientSourceTV(iClient))
		return;
	
	if(IsFakeClient(iClient))
	{
		g_iRank[iClient] = GetRandomInt(8, 17);
		CS_SetClientClanTag(iClient, g_sRanksShort[g_iRank[iClient]]);
		return;
	}
	
	char sBuffer[8];
	GetClientCookie(iClient, g_hCookieELO, sBuffer, sizeof(sBuffer));
	if(!StrEqual(sBuffer, ""))
	{
		g_iELO[iClient] = StringToInt(sBuffer);
		if(IsClientInGame(iClient) && GetClientTeam(iClient) > CS_TEAM_SPECTATOR)
			CS_SetClientClanTag(iClient, g_sRanksShort[g_iRank[iClient]]);
	}
	else 
	{
		g_iRank[iClient] = -1;
		if(IsClientInGame(iClient) && GetClientTeam(iClient) > CS_TEAM_SPECTATOR)
			CS_SetClientClanTag(iClient, "???");
	}
}

public void OnClientDisconnect(int iClient)
{
	g_iRank[iClient] = -1;
}

public Action CP_OnChatMessage(int& author, ArrayList recipients, char[] flagstring, char[] name, char[] message, bool& processcolors, bool& removecolors)
{
	if(g_iRank[author] != -1)
		Format(name, MAXLENGTH_NAME, "{lime}[%s]{teamcolor}%s", g_sRanksShort[g_iRank[author]], name);
	return Plugin_Changed;
}

/* Admin: Build Teams */

public Action Cmd_Teams(int iClient, int iArgs)
{
	BuildTeams(iClient);
	return Plugin_Handled;
}

void BuildTeams(int iClient)
{
	int iWait;
	LoopIngameClients(i)
	{
		if(g_iRank[i] == -1)
		{
			if(IsClientSourceTV(i) || IsFakeClient(i) || GetClientTeam(i) <= CS_TEAM_SPECTATOR)
				continue;
			
			Menu_Rank(i);
			iWait++;
		}
	}
	
	if(iWait > 0)
	{
		CReplyToCommand(iClient, "%s %i Spieler %s noch keinen Rank ausgesucht, Teams werden gemixt sobald alle einen Rang ausgewählt haben.", CPREFIX, iWait, iWait == 1 ? "hat" : "haben");
		g_bWait = true;
	}
	else MixTeams();
}

/* Rank Menu */

public Action Cmd_Rank(int iClient, int iArgs)
{
	Menu_Rank(iClient);
	return Plugin_Handled;
}

void Menu_Rank(int iClient)
{
	Menu menu = new Menu(MenuHandler_Vote);
	menu.SetTitle("Um möglichst faire Teams bilden zu können wähle bitte einen Rang aus der zu dir passt.\nDein Rang wird gespeichert und deiner Leistung entsprechend laufend angepasst.");
	
	for (int i = 0; i < 18; i++)
	{
		char sBuffer[8];
		IntToString(i, sBuffer, sizeof(sBuffer));
		menu.AddItem(sBuffer, g_sRanks[i], g_iRank[iClient] == i ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
	}
	
	SetMenuExitButton(menu, true);
	
	menu.Display(iClient, MENU_TIME_FOREVER);
}

bool SaveElo(int iClient)
{
	if(g_iELO[iClient] < 1)
		return false;
	
	char sBuffer[32];
	Format(sBuffer, sizeof(sBuffer), "%i", g_iELO[iClient]);
	SetClientCookie(iClient, g_hCookieELO, sBuffer);
	
	return true;
}

int GetRank(int iElo)
{
	for (int i = 0; i < 18; i++)
	{
		if(iElo < g_iRankELO[i])
			continue;
		
		return i;
	}
	
	return -1;
}

public int MenuHandler_Vote(Menu menu, MenuAction action, int iClient, int params)
{
	if (action == MenuAction_Select)
	{
		char sInfo[64];
		menu.GetItem(params, sInfo, sizeof(sInfo));
		
		g_iELO[iClient] = g_iRankELO[StringToInt(sInfo)];
		g_iRank[iClient] = GetRank(g_iELO[iClient]);
		SaveElo(iClient);
		
		CS_SetClientClanTag(iClient, g_sRanksShort[g_iRank[iClient]]);
		
		CPrintToChatAll("%s {purple}%N {yellow}hat zu {lime}\"%s\" {yellow}gewechselt.", CPREFIX, iClient, g_sRanks[g_iRank[iClient]]);
		
		int iWait;
		if(g_bWait)
		{
			LoopIngameClients(i)
			{
				if(IsClientSourceTV(i) || GetClientTeam(i) <= CS_TEAM_SPECTATOR)
					continue;
				
				if(g_iRank[i] == -1)
					iWait++;
			}
			
			if(iWait == 0)
				MixTeams();
		}
	}
	else if (action == MenuAction_End)
		delete menu;
}

void MixTeams()
{
	g_bWait = false;
	
	int iTrys, iElo, iEloCT, iEloT, iEloAvg, iEloAvgCT, iEloAvgT, iEloDiff, iEloDiff2;
	int iEloMinDiff = -1;
	
	// Available players
	ArrayList aPlayers = new ArrayList();
	
	// Final team buffer
	ArrayList aTeamCT = new ArrayList();
	ArrayList aTeamT = new ArrayList();
	
	// Temp team buffer
	ArrayList aCTs = new ArrayList();
	ArrayList aTs = new ArrayList();
	
	LoopIngameClients(iClient)
	{
		if(IsClientSourceTV(iClient) || GetClientTeam(iClient) <= CS_TEAM_SPECTATOR)
			continue;
		
		aPlayers.Push(iClient);
	}
	
	LoopArray(iIndex, aPlayers)
	{
		int iClient = aPlayers.Get(iIndex);
		iElo += g_iELO[g_iRank[iClient]];
	}
	
	while(iTrys < 1000)
	{
		iTrys++;
		iEloCT = 0;
		iEloT = 0;
		
		SortADTArray(aPlayers, Sort_Random, Sort_Integer);
		
		aCTs.Clear();
		aTs.Clear();
		
		LoopArray(iIndex, aPlayers)
		{
			int iClient = aPlayers.Get(iIndex);
			
			if(aCTs.Length == aTs.Length)
			{
				aCTs.Push(iClient);
				iEloCT += g_iELO[g_iRank[iClient]];
			}
			else if(aTs.Length < aCTs.Length)
			{
				aTs.Push(iClient);
				iEloT += g_iELO[g_iRank[iClient]];
			}
			else 
			{
				aCTs.Push(iClient);
				iEloCT += g_iELO[g_iRank[iClient]];
			}
		}
		
		iEloAvg = RoundToCeil(float(iElo) / float(aPlayers.Length));
		iEloAvgCT = RoundToCeil(float(iEloCT) / float(aCTs.Length));
		iEloAvgT = RoundToCeil(float(iEloT) / float(aTs.Length));
		
		iEloDiff = GetAbs(iEloAvgCT - iEloAvgT);
		iEloDiff2 = GetAbs(iEloAvg - iEloAvgCT);
		
		// Keep best sample if max trys exeeded
		int iMinNew = GetAbs(iEloDiff - iEloDiff2);
		if(iEloMinDiff == -1 || iEloMinDiff > iMinNew)
		{
			iEloMinDiff = iMinNew;
			aTeamCT.Clear();
			LoopArray(iIndex, aCTs)
				aTeamCT.Push(aCTs.Get(iIndex));
			
			aTeamT.Clear();
			LoopArray(iIndex, aTs)
				aTeamT.Push(aTs.Get(iIndex));
		}
	}
	
	LoopArray(iIndex, aTeamCT)
	{
		int iClient = aTeamCT.Get(iIndex);
		ChangeClientTeam(iClient, CS_TEAM_CT);
	}
	
	LoopArray(iIndex, aTeamT)
	{
		int iClient = aTeamT.Get(iIndex);
		ChangeClientTeam(iClient, CS_TEAM_T);
	}
	
	ServerCommand("mp_restartgame 1");
	
	delete aCTs;
	delete aTs;
	delete aPlayers;
	delete aTeamCT;
	delete aTeamT;
}

stock GetAbs(value)
{
	return (value ^ (value >> 31)) - (value >> 31);
}

public Action Event_PlayerDeath(Handle event, const char[] name, bool dontBroadcast)
{
	int iVictim = GetClientOfUserId(GetEventInt(event, "userid"));
	int iAttacker = GetClientOfUserId(GetEventInt(event, "attacker"));
	int iAssister = GetClientOfUserId(GetEventInt(event, "assister"));
	ELO(iAttacker, iVictim, iAssister);
	
	return Plugin_Continue;
}

/* Calc. ELO */

void ELO(int iAttacker, int iVictim, int iAssister)
{
	if(!iAttacker || !IsClientInGame(iAttacker) || !iVictim || !IsClientInGame(iVictim))
		return; // Ignore invalid players
	
	if(IsFakeClient(iAttacker))
		g_iELO[iAttacker] = 500; // bots count as low players
	
	if(IsFakeClient(iVictim))
		g_iELO[iVictim] = 500; // bots count as low players
	
	if(g_iELO[iAttacker] < 1 || g_iELO[iVictim] < 1)
		return; // player not loaded yet
	
	// Attacker
	
	int iEloDiff = CalcElo(iAttacker, iVictim);
	if(iEloDiff <= 0)
		return; // Get ELO difference
	
	g_iELO[iAttacker] += iEloDiff; // Give attacker ELO diff as points
	
	SaveElo(iAttacker);
	CheckRank(iAttacker);
	
	// Victim
	
	float fCapELO = 2000.0;	// scale ELO the victim can loose based on his current ELO
							// 1000 = *0.5 2000 = *1.0 4000 = *2.0
	int iEloLoose = 1 + RoundToFloor(float(iEloDiff) * (float(g_iELO[iVictim]) / fCapELO))
	if(iEloLoose > 30)		// Max ELO a player can loose
		iEloLoose = 30;
	
	g_iELO[iVictim] -= iEloLoose; // Decrease victims ELO
	
	if(g_iELO[iVictim] < 1) // Should not be possible xD
		g_iELO[iVictim] = 1; // Poor guy, ROFL
	
	SaveElo(iVictim);
	CheckRank(iVictim);
		
	// Assister
	
	if(iAssister && IsClientInGame(iAssister) && !IsFakeClient(iAssister))
	{
		int iAssisterEloDiff = RoundToFloor((float(CalcElo(iAssister, iVictim))*0.6)); // 60% efficiency
		
		if(iAssisterEloDiff < 1) // Min get +1
			iAssisterEloDiff = 1;
		
		g_iELO[iAssister] += iAssisterEloDiff;
		SaveElo(iAssister);
		CheckRank(iAssister);
	}
}

int CalcElo(int iAttacker, int iVictim)
{
	float fMaxDiff = 25.0; // Max ELO difference
	
	// Get ELO difference including attacker's & victim's implact scale
	float prob = 1/(Pow(10.0, (float(g_iELO[iVictim])-float(g_iELO[iAttacker]))/400)+1);
	return RoundToFloor(fMaxDiff*(1-prob));
}

bool CheckRank(int iClient)
{
	int iRankNew = GetRank(g_iELO[iClient])
	if(g_iRank[iClient] != iRankNew)
	{
		g_iRank[iClient] = iRankNew;
		CS_SetClientClanTag(iClient, g_sRanksShort[g_iRank[iClient]]);
	}
}

public void CallBack_Empty(Handle owner, Handle hndl, const char[] error, any userid)
{
	if (owner == null || hndl == null)
	{
		DatabaseError("CallBack_Empty", error);
		return;
	}
}

public void ZCore_Mysql_OnDatabaseError (int index, char[] config, char[] error )
{
	if(!StrEqual(config, "hg"))
		return;
	
	DatabaseError("ZCore_Mysql_OnDatabaseError", error);
}

void DatabaseError(const char[] sFunction, const char[] error)
{
	LogError("SQL Error on %s: %s", sFunction, error);
	
	g_iSQL = -1;
	g_hSQL = null;
	g_bConnected = false;
}

public void ZCore_Mysql_OnDatabaseConnected(int index, char[] config, Database connection_handle)
{
	if(!StrEqual(config, "hg"))
		return;
	
	g_iSQL = index;
	g_hSQL = connection_handle;
	
	g_bConnected = true;
	CreateTables();
}

void ConnectDB()
{
	if(!g_pZcoreMysql)
		return;
		
	if(g_hSQL != null)
		return;
	
	ZCore_Mysql_Connect("hg");
	
	if(g_hSQL != null)
	{
		g_bConnected = true;
		CreateTables();
	}
}

void CreateTables()
{
	SQL_SetCharset(g_hSQL, CHARSET);
	SQL_TQuery(g_hSQL, CallBack_Empty, "SET NAMES '" ... CHARSET ... "';");
	
	SQL_TQuery(g_hSQL, CallBack_Empty, sqlCreateTableRounds);
	SQL_TQuery(g_hSQL, CallBack_Empty, sqlCreateTablePlayers);
	
	SQL_TQuery(g_hSQL, CallBack_LoadPlayerCount, sqlLoadPlayerCount);
	SQL_TQuery(g_hSQL, CallBack_LoadLastRound, sqlLoadLastRound);
}